package main

import (
	"gitlab.com/stellarpower/go-runprocess"
)


func doTest(executable string, args []string, allInput string){
	returnCode, stdout, stderr := runprocess.RunProcessSynchronous(executable, args, allInput)
	print("ret:\t\t", returnCode, "\nstdout:\t\t", stdout, "\nstderr:\t\t", stderr, "\n")
}

func main() {
	doTest("date", []string{}, "")
	doTest("/bin/ls", []string{"-la", "/bin"}, "")
	doTest("/bin/cat", []string{}, "Hello\nworld\n...\n")
	doTest("/bin/false", []string{}, "Dummy - Not needed")
	// Add some more complex proceses
}
