package runprocess

import (
	"os/exec"	
	"sync"
	"io"
)

func RunProcessSynchronous(executable string, args []string, allInput string) (int, string, string){

	process := exec.Command(executable, args...)

	var threadWait sync.WaitGroup
	threadWait.Add(3)

	var stdout, stderr string

	inputPipe, e := process.StdinPipe() 
	if (e != nil) || (inputPipe == nil) {
		panic(e)
	}
	
	outputPipe, e := process.StdoutPipe() 
	if (e != nil) || (outputPipe == nil) {
		panic(e)
	}

	errorPipe, e := process.StderrPipe() 
	if (e != nil) || (errorPipe == nil) {
		panic(e)
	}

	process.Start()
	// Must be started before we read it.
	
    
	go func() {
		inputPipe.Write([]byte(allInput))
		inputPipe.Close()   
		threadWait.Done()
		
    }()
	
	go func() {
		bytes, e  := io.ReadAll(outputPipe)
		if e == nil {
			stdout = string(bytes)
		} else {
			panic(e)
		}
		threadWait.Done()
    }()
		
	go func() {
		bytes, e := io.ReadAll(errorPipe)
		if e == nil {
			stderr = string(bytes)
		} else {
			panic(e)
		}
		threadWait.Done()
    }()

    
	threadWait.Wait()
	exitError := process.Wait()
	exitStatus := 0

	if exitError != nil {
		if exitError, ok := exitError.(*exec.ExitError); ok {
			exitStatus = exitError.ExitCode()
		}
	}
	
    
	return exitStatus, stdout, stderr
}

// TODO: 
// Have an asynchronous version, possibly communicating via channels and streams, whatever is idiomatic to Go.
// As we already use three goroutines, we may be able to make minor modifications and call that as a goroutine to run in the background
// or normally to wait on the return, with something else waiting on the I/O in a separate thread.

